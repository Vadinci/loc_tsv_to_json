const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const Path = require('path');
const Url = require('url');

const menuTemplate = require('./menu');

var mainWindow;

var createMainWindow = function () {
    mainWindow = new BrowserWindow({
        width: 500,
        height: 500,
        useContentSize: true,
        resizable: false,
        webPreferences : {
            nodeIntegration : true
        }
    });

    mainWindow.loadURL(Url.format({
        pathname: Path.join(__dirname, '/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    mainWindow.on('closed', function () {
        mainWindow = undefined;
    });
};

app.on('ready', function () {
    var menu = electron.Menu.buildFromTemplate(menuTemplate);
    electron.Menu.setApplicationMenu(menu);

    createMainWindow();
});

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    if (!mainWindow) {
        createMainWindow()
    }
})