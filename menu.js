var pjson = require('./package.json');
const app = require('electron').app;

module.exports = [{
    label: 'File',
    submenu: []
}, {
    label: 'Developer',
    submenu: [{
        label: 'Developer Tools',
        accelerator: 'Alt+CmdOrCtrl+I',
        click: function (item, focusedWindow) {
            if (focusedWindow)
                focusedWindow.openDevTools();
        }
    }, {
        label: 'Refresh',
        role: 'reload',
        accelerator: 'CmdOrCtrl+R'
    }]
}, {
    label: 'Help',
    submenu: [{
        label: 'Version ' + pjson.version,
        enabled: false
    }]
}];


if (process.platform == 'darwin') {
    const name = require('electron').app.getName();
    module.exports.unshift({
        label: name,
        submenu: [{
            label: 'About ' + name,
            role: 'about'
        }, {
            type: 'separator'
        }, {
            label: 'Services',
            role: 'services',
            submenu: []
        }, {
            type: 'separator'
        }, {
            label: 'Hide ' + name,
            accelerator: 'Command+H',
            role: 'hide'
        }, {
            label: 'Hide Others',
            accelerator: 'Command+Shift+H',
            role: 'hideothers'
        }, {
            label: 'Show All',
            role: 'unhide'
        }, {
            type: 'separator'
        }, {
            label: 'Quit',
            accelerator: 'Command+Q',
            click: function () {
                app.quit();
            }
        }, ]
    });
}