 const FS = require('fs');
 const Path = require('path');

 module.exports = function (_path) {

     var path = _path;
     var name = Path.basename(path, '.json');

     var save = function (content, onComplete) {
         FS.writeFile(path, content, 'utf8', function (err) {
             if (err) {
                 console.log(err);
                 return;
             }
             if (onComplete) {
                 onComplete(content);
             }
         });
     };

     var load = function (onComplete) {
         FS.readFile(path, 'utf8', function (err, data) {
             if (err) {
                 //console.log(err);
                 onComplete(undefined);
                 return;
             }
             if (onComplete) {
                 onComplete(data);
             }
         });
     };

     var prettyStringify = function (json) {
         var stringy = JSON.stringify(json);
         var lines;
         var ii;
         var jj;
         var indent = 0;

         //do all the linebreaks and spaces. Regex time!
         stringy = stringy.replace(/([\[,{;][\w\[,{;]*)([^ \t])/g, '$1\n$2');
         stringy = stringy.replace(/([\]}][\w\]]*)/g, '\n$&');
         stringy = stringy.replace(/,([^\n\s])/g, ', $1');
         stringy = stringy.replace(/(^\n)}/g, '$1\n}');
         stringy = stringy.replace(/:/g, ': ');

         //insert indentation
         lines = stringy.split('\n');
         for (ii = 0; ii < lines.length; ii++) {
             if (/[\]}]/.test(lines[ii])) {
                 indent--;
             }
             for (jj = 0; jj < indent; jj++) {
                 lines[ii] = '\t' + lines[ii];
             }
             if (/[\[{]/.test(lines[ii])) {
                 indent++;
             }
         }

         stringy = lines.join('\n');
         return stringy;
     };

     return {
         save: save,
         load: load,
         prettyStringify: prettyStringify,
         getName: function () {
             return name;
         }
     };
 };