# Localization TSV to Json

A simple tool (run in Electron) to convert a .tsv (tab separated values file) file to a set of .json files into a specific output folder


## Example
The input
```
KEY	EN	NL
play	Play	Speel
quit	Quit	Stop
```

Creates two files, `en.json` and `nl.json`:
```
{
	"play": "Play",
	"quit": "Quit"
}
```
```
{
	"play": "Speel",
	"quit": "Stop"
}
```

### TODO
Pick a character that should not be used in any normal text, and use it as an array separator.

Separate the code into more manageable modules

General UI improvements
