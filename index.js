const File = require('./file');
const Dialog = require('electron').remote.dialog;


var path = undefined;

var dropper = document.getElementById('dropper');

var languages = {};

var btnExport = document.getElementById('export');

btnExport.disabled = !false;

dropper.addEventListener('drop', function (evt) {
    evt.preventDefault();
    // If dropped items aren't files, reject them
    var dt = evt.dataTransfer;
    if (dt.items) {
        // Use DataTransferItemList interface to access the file(s)
        for (var i = 0; i < dt.items.length; i++) {
            if (dt.items[i].kind == "file") {
                var f = dt.items[i].getAsFile();
                parseSource(f.path);
                return;
            }
        }
    } else {
        // Use DataTransfer interface to access the file(s)
        for (var i = 0; i < dt.files.length; i++) {
            parseSource(f.path);
            return;
        }
    }
});

dropper.addEventListener('dragover', function (evt) {
    evt.preventDefault();
});

btnExport.addEventListener('click', function (evt) {
    evt.preventDefault();

    Dialog.showOpenDialog({
        properties: ['openDirectory']
    }).then(result => {
        if (result.cancelled) return;
        if (result.filePaths.length === 0) return;

        let path = result.filePaths[0];
        var key;
        var file;
        var stringy;
        for (key in languages) {
            file = new File(path + '/' + key.toLowerCase() + '.json');
            //stringy = file.prettyStringify(languages[key]);
            file.save(JSON.stringify(languages[key], null, 2));
        }
    });
});

var parseSource = function (path) {
    languages = {};

    var sourceFile = new File(path);

    document.getElementById('path').innerText = path;

    var parse = function (data) {
        data = data.replace(/\r\n/g, '\n').replace(/\r/g, '\n');

        var rows = data.split('\n');

        var row;
        var ii;
        var jj;
        var key;
        var values;
        var langs = rows[0].split('\t');

        //remove any completely blank lines
        for (ii = rows.length - 1; ii >= 1; ii--) {
            row = rows[ii];
            if (!/\S/.test(row)) {
                //only has whitespace
                rows.splice(ii, 1);
            }
        }

        var lang;
        var skipCols = [];
        var writeLanguages = [];

        langs.shift();
        for (ii = langs.length - 1; ii >= 0; ii--) {
            langs[ii] = langs[ii].replace(/\s/g, '');
            lang = langs[ii];

            if (lang.indexOf('!') === 0) {
                skipCols.push(ii);
                continue;
            };
            writeLanguages.push(lang);
            languages[lang] = {};
        }
        document.getElementById('langs').innerText = writeLanguages.join(', ');

        var line;

        for (ii = 1; ii < rows.length; ii++) {
            values = rows[ii].split('\t');
            key = values.shift();

            for (jj = 0; jj < values.length; jj++) {
                if (skipCols.indexOf(jj) !== -1) continue;

                lang = langs[jj];

                line = values[jj];
                line = line.replace(/\\n/g, '\n');
                languages[langs[jj]][key] = line;

                if (!line) {
                    delete languages[langs[jj]][key];
                }
            }
        }

        document.getElementById('lines').innerText = rows.length - 1;
        btnExport.disabled = !true;
    };

    sourceFile.load(parse);
    btnExport.disabled = !false;
};